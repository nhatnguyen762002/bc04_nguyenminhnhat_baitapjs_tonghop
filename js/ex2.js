var numArr = [];

function kiemTraSNT(number) {
  if (number < 2) {
    return 0;
  }

  for (var i = 2; i < number; i++) {
    if (number % i == 0) {
      return 0;
    }
  }
  return 1;
}

function themSo() {
  var numValue = document.querySelector("#txt-number").value * 1;
  document.querySelector("#txt-number").value = "";

  var mangEl = document.querySelector("#txt-mang");

  numArr.push(numValue);
  mangEl.innerText = numArr.join(", ");
}

function timSNT() {
  document.querySelector("#txt-ket-qua").innerText = ``;

  for (var i = 0; i < numArr.length; i++) {
    if (kiemTraSNT(numArr[i]) == 1) {
      document.querySelector("#txt-ket-qua").innerHTML += `${numArr[i]}, `;
    }
  }
}
