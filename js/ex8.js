baiArr = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
userArr = [[], [], [], []];

var baiEl = document.querySelector("#txt-bai");

baiEl.innerHTML = baiArr.join(", ");

function chiaBai() {
  var user1El = document.querySelector("#txt-user1");
  var user2El = document.querySelector("#txt-user2");
  var user3El = document.querySelector("#txt-user3");
  var user4El = document.querySelector("#txt-user4");

  baiArr.forEach(function (bai) {
    for (var i = 0; i < userArr.length; i++) {
      userArr[i].push(baiArr.shift());
    }
  });

  user1El.innerHTML = `${userArr[0]}`;
  user2El.innerHTML = `${userArr[1]}`;
  user3El.innerHTML = `${userArr[2]}`;
  user4El.innerHTML = `${userArr[3]}`;

  baiEl.innerHTML = baiArr.join(", ");
}
