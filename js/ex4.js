function timUocSo() {
  var numValue = document.querySelector("#txt-number").value * 1;
  var ketQuaEl = document.querySelector("#txt-ket-qua");

  ketQuaEl.innerHTML = `Ước số của ${numValue} là: `;

  for (var i = numValue; i >= 1; i--) {
    if (numValue % i == 0) {
      ketQuaEl.innerHTML += `${i}, `;
    }
  }
}
