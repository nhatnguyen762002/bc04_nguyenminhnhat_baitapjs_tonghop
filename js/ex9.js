function ketQua() {
  var conValue = document.querySelector("#txt-con").value * 1;
  var chanValue = document.querySelector("#txt-chan").value * 1;
  var ketQuaEl = document.querySelector("#txt-ket-qua");

  for (var i = 1; i <= conValue; i++) {
    if (i * 2 + (conValue - i) * 4 == chanValue) {
      ketQuaEl.innerHTML = `${i} con gà, ${conValue - i} con chó`;
    }
  }
}
