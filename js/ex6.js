function timX() {
  var ketQuaEl = document.querySelector("#txt-ket-qua");
  var result = 0;

  for (var i = 1; result <= 100; i++) {
    result += i;

    ketQuaEl.innerHTML = `x = ${i - 1}`;
  }
}

timX();
