function tinhS() {
  var numValue = document.querySelector("#txt-number").value * 1;
  var ketQuaEl = document.querySelector("#txt-ket-qua");

  var result = 0;

  for (var i = 2; i <= numValue; i++) {
    result += i;
  }
  result += 2 * numValue;

  ketQuaEl.innerHTML = result;
}
