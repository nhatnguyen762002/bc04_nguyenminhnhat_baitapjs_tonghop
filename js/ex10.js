function tinhGio(gio) {
  let gioReg = 0;
  for (let i = 0; i < gio; i++) {
    gioReg += 30;
  }
  return gioReg;
}

function tinhPhut(phut) {
  let phutReg = 0;
  for (let i = 0; i < phut; i++) {
    phutReg += 6;
  }
  return phutReg;
}

function tinhGoc() {
  let gioValue = document.querySelector("#txt-gio").value * 1;
  let phutValue = document.querySelector("#txt-phut").value * 1;
  let ketQuaEl = document.querySelector("#txt-ket-qua");

  let gioReg = tinhGio(gioValue);
  let phutReg = tinhPhut(phutValue);

  for (let i = 0; i < phutValue; i++) {
    gioReg += 0.5;
  }

  ketQuaEl.innerHTML = `Góc 1 là ${Math.abs(gioReg - phutReg)} độ. Góc 2 là ${
    360 - Math.abs(gioReg - phutReg)
  } độ`;
}
